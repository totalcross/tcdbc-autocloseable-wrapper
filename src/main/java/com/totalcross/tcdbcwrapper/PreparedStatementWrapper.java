package com.totalcross.tcdbcwrapper;
import java.sql.SQLException;
import java.sql.SQLWarning;

import totalcross.sql.Connection;
import totalcross.sql.PreparedStatement;
import totalcross.sql.ResultSetMetaData;
import totalcross.sql.Timestamp;
import totalcross.sys.Time;
import totalcross.sys.Vm;
import totalcross.util.BigDecimal;
import totalcross.util.Date;

public class PreparedStatementWrapper implements PreparedStatement, AutoCloseable {
	PreparedStatement pstmt;
	public final int id;
	
	public static PreparedStatementWrapper getPreparedStatementWrapper(PreparedStatement pstmt) {
		if (pstmt instanceof PreparedStatementWrapper) {
			return (PreparedStatementWrapper) pstmt;
		} else {
			return new PreparedStatementWrapper(pstmt);
		}
	}
	
	public PreparedStatementWrapper(PreparedStatement pstmt) {
		this.pstmt = pstmt;
		id = IdManager.getId();
		
		Vm.debug("Created PreparedStatement id " + id);
	}
	
	@Override
	public void addBatch(String arg0) throws SQLException {
		pstmt.addBatch();
	}

	@Override
	public void cancel() throws SQLException {
		pstmt.cancel();
	}

	@Override
	public void clearBatch() throws SQLException {
		pstmt.clearBatch();
	}

	@Override
	public void clearWarnings() throws SQLException {
		pstmt.clearWarnings();
	}

	@Override
	public void close() throws SQLException {
		pstmt.close();
		
		Vm.debug("Closed PreparedStatement id " + id);
	}

	@Override
	public boolean execute(String arg0) throws SQLException {
		return pstmt.execute();
	}

	@Override
	public int[] executeBatch() throws SQLException {
		return pstmt.executeBatch();
	}

	@Override
	public ResultSetWrapper executeQuery(String arg0) throws SQLException {
		return ResultSetWrapper.getResultSetWrapper(pstmt.executeQuery(arg0));
	}

	@Override
	public int executeUpdate(String arg0) throws SQLException {
		return pstmt.executeUpdate(arg0);
	}

	@Override
	public Connection getConnection() throws SQLException {
		return pstmt.getConnection();
	}

	@Override
	public int getFetchDirection() throws SQLException {
		return pstmt.getFetchDirection();
	}

	@Override
	public int getFetchSize() throws SQLException {
		return pstmt.getFetchSize();
	}

	@Override
	public int getMaxRows() throws SQLException {
		return pstmt.getMaxRows();
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		return pstmt.getMoreResults();
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		return pstmt.getQueryTimeout();
	}

	@Override
	public ResultSetWrapper getResultSet() throws SQLException {
		return ResultSetWrapper.getResultSetWrapper(pstmt.getResultSet());
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		return pstmt.getResultSetConcurrency();
	}

	@Override
	public int getResultSetType() throws SQLException {
		return pstmt.getResultSetType();
	}

	@Override
	public int getUpdateCount() throws SQLException {
		return pstmt.getUpdateCount();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return pstmt.getWarnings();
	}

	@Override
	public void setCursorName(String arg0) throws SQLException {
		pstmt.setCursorName(arg0);
	}

	@Override
	public void setFetchDirection(int arg0) throws SQLException {
		pstmt.setFetchDirection(arg0);
	}

	@Override
	public void setFetchSize(int arg0) throws SQLException {
		pstmt.setFetchSize(arg0);
	}

	@Override
	public void setMaxRows(int arg0) throws SQLException {
		pstmt.setMaxRows(arg0);
	}

	@Override
	public void setQueryTimeout(int arg0) throws SQLException {
		pstmt.setQueryTimeout(arg0);
	}

	@Override
	public void addBatch() throws SQLException {
		pstmt.addBatch();
	}

	@Override
	public void clearParameters() throws SQLException {
		pstmt.clearParameters();
	}

	@Override
	public boolean execute() throws SQLException {
		return pstmt.execute();
	}

	@Override
	public ResultSetWrapper executeQuery() throws SQLException {
		return ResultSetWrapper.getResultSetWrapper(pstmt.executeQuery());
	}

	@Override
	public int executeUpdate() throws SQLException {
		return pstmt.executeUpdate();
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		return pstmt.getMetaData();
	}

	@Override
	public void setBigDecimal(int arg0, BigDecimal arg1) throws SQLException {
		pstmt.setBigDecimal(arg0, arg1);
	}

	@Override
	public void setBoolean(int arg0, boolean arg1) throws SQLException {
		pstmt.setBoolean(arg0, arg1);
	}

	@Override
	public void setByte(int arg0, byte arg1) throws SQLException {
		pstmt.setByte(arg0, arg1);
	}

	@Override
	public void setBytes(int arg0, byte[] arg1) throws SQLException {
		pstmt.setBytes(arg0, arg1);
	}

	@Override
	public void setDate(int arg0, Date arg1) throws SQLException {
		pstmt.setDate(arg0, arg1);
	}

	@Override
	public void setDouble(int arg0, double arg1) throws SQLException {
		pstmt.setDouble(arg0, arg1);
	}

	@Override
	public void setInt(int arg0, int arg1) throws SQLException {
		pstmt.setInt(arg0, arg1);
	}

	@Override
	public void setLong(int arg0, long arg1) throws SQLException {
		pstmt.setLong(arg0, arg1);
	}

	@Override
	public void setNull(int arg0, int arg1, String arg2) throws SQLException {
		pstmt.setNull(arg0, arg1, arg2);
	}

	@Override
	public void setNull(int arg0, int arg1) throws SQLException {
		pstmt.setNull(arg0, arg1);
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2, int arg3) throws SQLException {
		pstmt.setObject(arg0, arg1, arg2, arg3);
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2) throws SQLException {
		pstmt.setObject(arg0, arg1, arg2);
	}

	@Override
	public void setObject(int arg0, Object arg1) throws SQLException {
		pstmt.setObject(arg0, arg1);
	}

	@Override
	public void setShort(int arg0, short arg1) throws SQLException {
		pstmt.setShort(arg0, arg1);
	}

	@Override
	public void setString(int arg0, String arg1) throws SQLException {
		pstmt.setString(arg0, arg1);
	}

	@Override
	public void setTime(int arg0, Time arg1) throws SQLException {
		pstmt.setTime(arg0, arg1);
	}

	@Override
	public void setTimestamp(int arg0, Timestamp arg1) throws SQLException {
		pstmt.setTimestamp(arg0, arg1);
	}

}
