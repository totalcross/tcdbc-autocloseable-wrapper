package com.totalcross.tcdbcwrapper;
public final class IdManager {
	private static int nextId = 0;
	private IdManager() {}
	
	public static int getId() {
		return nextId++;
	}
}
