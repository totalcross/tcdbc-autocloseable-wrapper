package com.totalcross.tcdbcwrapper;
import java.sql.SQLException;
import java.sql.SQLWarning;

import totalcross.sql.Connection;
import totalcross.sys.Vm;

public class ConnectionWrapper implements Connection, AutoCloseable {
	final Connection conn;
	public final int id;
	
	public static ConnectionWrapper getConnectionWrapper(Connection conn) {
		if (conn instanceof ConnectionWrapper) {
			return (ConnectionWrapper) conn;
		} else {
			return new ConnectionWrapper(conn);
		}
	}
	
	public ConnectionWrapper(Connection conn) {
		this.conn = conn;
		id = IdManager.getId();
		
		Vm.debug("Created Connection id " + id);
	}
	
	@Override
	public void clearWarnings() throws SQLException {
		conn.clearWarnings();
	}

	@Override
	public void close() throws SQLException {
		conn.close();
		
		Vm.debug("Closed Connection id " + id);
	}

	@Override
	public void commit() throws SQLException {
		conn.commit();
	}

	@Override
	public StatementWrapper createStatement() throws SQLException {
		return StatementWrapper.getStatementWrapper(conn.createStatement());
	}

	@Override
	public StatementWrapper createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
		return StatementWrapper.getStatementWrapper(conn.createStatement(resultSetType, resultSetConcurrency));
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		return conn.getAutoCommit();
	}

	@Override
	public String getCatalog() throws SQLException {
		return conn.getCatalog();
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		return conn.getTransactionIsolation();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return conn.getWarnings();
	}

	@Override
	public boolean isClosed() throws SQLException {
		return conn.isClosed();
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		return conn.isReadOnly();
	}

	@Override
	public String nativeSQL(String arg0) {
		return conn.nativeSQL(arg0);
	}

	@Override
	public PreparedStatementWrapper prepareStatement(String arg0) throws SQLException {
		return PreparedStatementWrapper.getPreparedStatementWrapper(conn.prepareStatement(arg0));
	}

	@Override
	public PreparedStatementWrapper prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
		return PreparedStatementWrapper.getPreparedStatementWrapper(conn.prepareStatement(sql,  resultSetType, resultSetConcurrency));
	}

	@Override
	public void rollback() throws SQLException {
		conn.rollback();
	}

	@Override
	public void setAutoCommit(boolean arg0) throws SQLException {
		conn.setAutoCommit(arg0);
	}

	@Override
	public void setCatalog(String arg0) throws SQLException {
		conn.setCatalog(arg0);
	}

	@Override
	public void setReadOnly(boolean arg0) throws SQLException {
		conn.setReadOnly(arg0);
	}

	@Override
	public void setTransactionIsolation(int arg0) throws SQLException {
		conn.setTransactionIsolation(arg0);
	}

}
