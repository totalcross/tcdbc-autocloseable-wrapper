package com.totalcross.tcdbcwrapper;
import java.sql.SQLException;
import java.sql.SQLWarning;

import totalcross.sql.Connection;
import totalcross.sql.Statement;
import totalcross.sys.Vm;

public class StatementWrapper implements Statement, AutoCloseable {
	final Statement stmt;
	public final int id;
	
	public static StatementWrapper getStatementWrapper(Statement stmt) {
		if (stmt instanceof StatementWrapper) {
			return (StatementWrapper) stmt;
		} else {
			return new StatementWrapper(stmt);
		}
	}
	
	public StatementWrapper(Statement stmt) {
		this.stmt = stmt;
		id = IdManager.getId();
		
		Vm.debug("Created Statement id " + id);
	}
	
	@Override
	public void addBatch(String arg0) throws SQLException {
		stmt.addBatch(arg0);
	}

	@Override
	public void cancel() throws SQLException {
		stmt.cancel();
	}

	@Override
	public void clearBatch() throws SQLException {
		stmt.clearBatch();
	}

	@Override
	public void clearWarnings() throws SQLException {
		stmt.clearWarnings();
	}

	@Override
	public void close() throws SQLException {
		stmt.close();
		
		Vm.debug("Closed Statement id " + id);
	}

	@Override
	public boolean execute(String arg0) throws SQLException {
		return stmt.execute(arg0);
	}

	@Override
	public int[] executeBatch() throws SQLException {
		return stmt.executeBatch();
	}

	@Override
	public ResultSetWrapper executeQuery(String arg0) throws SQLException {
		return ResultSetWrapper.getResultSetWrapper(stmt.executeQuery(arg0));
	}

	@Override
	public int executeUpdate(String arg0) throws SQLException {
		return stmt.executeUpdate(arg0);
	}

	@Override
	public Connection getConnection() throws SQLException {
		return new ConnectionWrapper(stmt.getConnection());
	}

	@Override
	public int getFetchDirection() throws SQLException {
		return stmt.getFetchDirection();
	}

	@Override
	public int getFetchSize() throws SQLException {
		return stmt.getFetchSize();
	}

	@Override
	public int getMaxRows() throws SQLException {
		return stmt.getMaxRows();
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		return stmt.getMoreResults();
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		return stmt.getQueryTimeout();
	}

	@Override
	public ResultSetWrapper getResultSet() throws SQLException {
		return ResultSetWrapper.getResultSetWrapper(stmt.getResultSet());
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		return stmt.getResultSetConcurrency();
	}

	@Override
	public int getResultSetType() throws SQLException {
		return stmt.getResultSetType();
	}

	@Override
	public int getUpdateCount() throws SQLException {
		return stmt.getUpdateCount();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return stmt.getWarnings();
	}

	@Override
	public void setCursorName(String arg0) throws SQLException {
		stmt.setCursorName(arg0);
	}

	@Override
	public void setFetchDirection(int arg0) throws SQLException {
		stmt.setFetchDirection(arg0);
	}

	@Override
	public void setFetchSize(int arg0) throws SQLException {
		stmt.setFetchSize(arg0);
	}

	@Override
	public void setMaxRows(int arg0) throws SQLException {
		stmt.setMaxRows(arg0);
	}

	@Override
	public void setQueryTimeout(int arg0) throws SQLException {
		stmt.setQueryTimeout(arg0);
	}

}
